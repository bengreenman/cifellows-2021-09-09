Title:
Types for Alloy: From Programming to Modelling

Description:
I'm currently designing a type system for the Alloy modelling language.

The purpose of the type system is to help programmers start writing models
(which are quite different from programs in subtle ways) while avoiding common
pitfalls. It's like training wheels. Eventually, people will turn off the type
system and switch to full Alloy.


#### Examples

- Knuth elevator in C++
  lol trouble right from the start with these include lines
  https://github.com/Quuxplusone/KnuthElevator/blob/main/cxx14.cpp

- Knuth elevator in Go
  ... trouble from the start here too, but the helpful comments are in the way
  https://github.com/meatfighter/knuth-elevator/blob/master/main/knuthElevator.go


